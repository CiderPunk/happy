"use strict";
var Express = require("express");
var Server = (function () {
    function Server(port) {
        if (port === void 0) { port = process.env.PORT || 80; }
        this.app = Express();
        //this.app.use('/src', Express.static('src'));
        this.app.use('/node_modules', Express.static('node_modules'));
        this.app.use(Express.static('public'));
        this.server = this.app.listen(port, function () {
            console.log('listening on *:' + port);
        });
    }
    return Server;
}());
exports.Server = Server;

//# sourceMappingURL=server.js.map
